﻿using CustomToDo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomToDo.BAL.Repository
{
    public interface ITaskAttachment : IRepository<TaskAttachment>
    {
        List<TaskAttachment> GetTaskAttachmentsById(int id);
    }
}
