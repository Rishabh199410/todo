﻿using CustomToDo.BAL.Repository;
using CustomToDo.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomToDo.BAL.Services
{
    public class TaskAttachmentServices : GenericRepository<admin_todoEntities, TaskAttachment>, ITaskAttachment
    {
        public List<TaskAttachment> GetTaskAttachmentsById(int id)
        {
            List<TaskAttachment> attachmentsList = null;
            try
            {
                var db = new admin_todoEntities();

                attachmentsList = db.TaskAttachments.Where(x => x.TaskId == id).ToList();
            }
            catch(Exception ex)
            {
                //throw ex;
            }
            return attachmentsList;
        }
    }
}
