﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CustomToDo.BAL.Repository;

namespace CustomToDo.BAL.Services
{
    public abstract class GenericRepository<C, T> : IRepository<T> where T : class where C : DbContext, new()
    {

        private C _entities = new C();
        public C Context
        {
            get { return _entities; }
            set { _entities = value; }
        }

        public void Add(T entity)
        {
            EnsureAttachedEF(entity);
            _entities.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            _entities.Set<T>().Remove(entity);
        }

        public void Edit(T entity)
        {

            _entities.Entry(entity).State = EntityState.Modified;
        }

        public void AddRange(IEnumerable<T> entity)
        {
            _entities.Set<T>().AddRange(entity);
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = _entities.Set<T>().Where(predicate);
            return query;
        }

        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = _entities.Set<T>();
            return query;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }

        public DbEntityEntry<T> EnsureAttachedEF(T entity)
        {
            var e = _entities.Entry(entity);
            if (e.State == EntityState.Detached)
            {
                _entities.Set<T>().Attach(entity);
                e = _entities.Entry(entity);
            }

            return e;
        }

    }
}
