﻿using CustomToDo.DAL;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using CustomToDo.Web.Models;

namespace CustomToDo.Web.HelperClass
{
    public class CustomToDoUtility
    {
        public enum Priorities
        {
            Normal = 1,
            Low = 2,
            High = 3,
            Important = 4
        }
        public enum Statuses
        {
            open = 1,
            close = 2
        }

        public enum Role
        {
            Contributor = 1,
            Admin = 2,
            Manager = 3,
        }

        public static AppUser CurrentLoginUser => GetLoggedInUser();

        private static HttpSessionState Session
        {
            get
            {
                return HttpContext.Current.Session;
            }
        }

        private static AppUser GetLoggedInUser()
        {
            if(Session["CurrentUser"] != null)
                return (AppUser)Session["CurrentUser"];
            else
                return null;
        }
    }
}