﻿using CustomToDo.BAL.Repository;
using CustomToDo.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomToDo.Web.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin/Admin

        public ITask _task;
        public IAppUser _appUser;

        public AdminController(ITask task, IAppUser appUser)
        {
            _task = task;
            _appUser = appUser;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View(new AdminModel());
        }

        [HttpPost]
        public ActionResult Create(AdminModel model)
        {
            if (ModelState.IsValid)
            {
                DAL.AppUser appUser = new DAL.AppUser();
                appUser.Firstname = model.Firstname;
                appUser.Lastname = model.Lastname;
                appUser.Email = model.Email;
                appUser.Username = model.Username;
                appUser.Password = model.Password;
                appUser.IsActive = model.IsActive;
                appUser.RoleId = model.RoleId;
                appUser.CreatedOn = model.CreatedOn;
                _appUser.Add(appUser);
                _appUser.Save();
                return RedirectToAction("DisplayUser");
            }
            else
            {
                return View();
            }
        }

        public ActionResult DisplayUser()
        {
            List<AdminModel> userlist = new List<AdminModel>();
            userlist = _appUser.GetAll().Select(x => new AdminModel()
            {
                Id = x.Id,
                Firstname = x.Firstname,
                Lastname = x.Lastname,
                Email = x.Email,
                Username = x.Username,
                Password = x.Password,
                RoleId = x.RoleId,
                IsActive = x.IsActive,
                CreatedOn = x.CreatedOn
            }).ToList();

            return View(userlist);
        }

        public ActionResult Edit(int id)
        {
            AdminModel adminModel = new AdminModel();
            var user = _appUser.FindBy(x => x.Id == id).FirstOrDefault();
            if (user != null)
            {
                adminModel.Id = user.Id;
                adminModel.Firstname = user.Firstname;
                adminModel.Lastname = user.Lastname;
                adminModel.Username = user.Username;
                adminModel.Email = user.Email;
                adminModel.Password = user.Password;
                adminModel.IsActive = user.IsActive;
                adminModel.RoleId = user.RoleId;
                adminModel.CreatedOn = user.CreatedOn;
            }
            return View(adminModel);
        }
        [HttpPost]
        public ActionResult Edit(AdminModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _appUser.FindBy(x => x.Id == model.Id).FirstOrDefault();
                if (user != null)
                {
                    user.Firstname = model.Firstname;
                    user.Lastname = model.Lastname;
                    user.Email = model.Email;
                    user.Username = model.Username;
                    user.Password = model.Password;
                    user.IsActive = model.IsActive;
                    user.RoleId = model.RoleId;
                    user.CreatedOn = model.CreatedOn;
                }
                _appUser.Edit(user);
                _appUser.Save();
                return RedirectToAction("DisplayUser");
            }
            else
            {
                return View();
            }
        }
        public ActionResult Delete(int id)
        {
            AdminModel adminModel = new AdminModel();
            var user = _appUser.FindBy(x => x.Id == id).FirstOrDefault();
            if (user != null)
            {
                adminModel.Id = user.Id;
                adminModel.Firstname = user.Firstname;
                adminModel.Lastname = user.Lastname;
                adminModel.Username = user.Username;
                adminModel.Email = user.Email;
                adminModel.Password = user.Password;
                adminModel.IsActive = user.IsActive;
                adminModel.RoleId = user.RoleId;
                adminModel.CreatedOn = user.CreatedOn;
            }
            return View(adminModel);
        }
        [HttpPost]
        public ActionResult Delete(AdminModel model)
        {
            try
            {
                var user = _appUser.FindBy(x => x.Id == model.Id).FirstOrDefault();
                _appUser.Delete(user);
                _appUser.Save();
                return RedirectToAction("DisplayUser");
            }
            catch
            {
                return View();
            }
        }
    }
}