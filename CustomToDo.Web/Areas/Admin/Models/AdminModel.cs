﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomToDo.Web.Models
{
    public class AdminModel
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }


    }
}