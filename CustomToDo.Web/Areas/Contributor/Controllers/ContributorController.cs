﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CustomToDo.BAL.Repository;
using CustomToDo.DAL;
using CustomToDo.Web.Areas.Contributor.Models;
using CustomToDo.Web.HelperClass;
using CustomToDo.Web.Models;


namespace CustomToDo.Web.Areas.Contributor.Controllers
{
    public class ContributorController : Controller
    {
        public ITask _task;
        public IAppUser _appUser;
        public ITaskAttachment _taskAttachment;

        public ContributorController(ITask task, IAppUser appUser, ITaskAttachment taskAttachment)
        {
            _task = task;
            _appUser = appUser;
            _taskAttachment = taskAttachment;
        }

        // GET: Contributor/Contributor
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MyDay(string sortOrder)
        {
            ViewBag.sortImportant = String.IsNullOrEmpty(sortOrder) ? "imp_desc" : "";
            ViewBag.sortDue = String.IsNullOrEmpty(sortOrder) ? "duedate_desc" : "";
            ViewBag.sortComplete = String.IsNullOrEmpty(sortOrder) ? "complete_desc" : "";
            ViewBag.sortMyday = String.IsNullOrEmpty(sortOrder) ? "myday_desc" : "";
            ViewBag.sortAlpha = String.IsNullOrEmpty(sortOrder) ? "alpha_desc" : "";
            ViewBag.sortCreate = String.IsNullOrEmpty(sortOrder) ? "create_desc" : "";

            TaskPageModel model = new TaskPageModel();

            #region Page Tasks
            List<TaskModel> list = new List<TaskModel>();
            var gettasklists = _task.GetAll().Where(x => x.DueDate == DateTime.Today && x.IsDeleted == false && x.ParentId == null).ToList();
            foreach(var gettask in gettasklists)
            {
                TaskModel task = new TaskModel();
                task.Id = gettask.Id;
                task.Title = gettask.Title;
                task.Priority = gettask.Priority;
                task.Status = gettask.Status;
                task.AssignedTo = gettask.AssignedTo;
                task.CreatedOn = gettask.CreatedOn;
                task.Creator = gettask.Creator;
                task.LastModifiedOn = gettask.LastModifiedOn;
                task.UpdatedBy = gettask.UpdatedBy;
                task.DueDate = gettask.DueDate;
                task.Comments = gettask.Comments;
                task.ParentId = gettask.ParentId ?? 0;

                var subtasklist = _task.GetAll().Where(x => x.DueDate > DateTime.Today && x.IsDeleted == false && x.ParentId == gettask.Id).ToList();
                List<TaskModel> sublist = new List<TaskModel>();
                foreach(var getsubtask in subtasklist)
                {
                    TaskModel subtask = new TaskModel();
                    subtask.Id = getsubtask.Id;
                    subtask.Title = getsubtask.Title;
                    subtask.Priority = getsubtask.Priority;
                    subtask.Status = getsubtask.Status;
                    subtask.AssignedTo = getsubtask.AssignedTo;
                    subtask.CreatedOn = getsubtask.CreatedOn;
                    subtask.Creator = getsubtask.Creator;
                    subtask.LastModifiedOn = getsubtask.LastModifiedOn;
                    subtask.UpdatedBy = getsubtask.UpdatedBy;
                    subtask.DueDate = getsubtask.DueDate;
                    subtask.Comments = getsubtask.Comments;
                    sublist.Add(subtask);
                }
                task.SubTaskList = sublist;

                list.Add(task);
            }


            foreach(var listitem in list)
            {
                listitem.SubTaskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id).Count();
                listitem.taskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id && l.Status == (int)CustomToDoUtility.Statuses.close).Count();
            }

            switch(sortOrder)
            {
                case "imp_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Priority).ToList();
                    }
                    catch(Exception ex) { }
                    break;
                case "duedate_desc":
                    try
                    {
                        list = list.OrderBy(x => x.DueDate).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "complete_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Status).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "myday_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.DueDate).ThenBy(x => x.DueDate == DateTime.Today).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "alpha_desc":
                    try
                    {
                        list = list.OrderBy(x => x.Title).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "create_desc":
                    try
                    {
                        list = list.OrderBy(x => x.CreatedOn).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                default:
                    break;
            }
            model.PageTasks = list;
            #endregion

            model.TotalTasks = _task.GetAll().Where(x => x.IsDeleted == false && x.ParentId == null).Select(x => new TaskModel()
            {
                Id = x.Id,
                Title = x.Title,
                Priority = x.Priority,
                Status = x.Status,
                AssignedTo = x.AssignedTo,
                CreatedOn = x.CreatedOn,
                Creator = x.Creator,
                LastModifiedOn = x.LastModifiedOn,
                UpdatedBy = x.UpdatedBy,
                DueDate = x.DueDate,
                Comments = x.Comments

            }).OrderByDescending(x => x.CreatedOn).ToList();

            return View(model);
        }

        public ActionResult Planned(string sortOrder)
        {
            ViewBag.sortImportant = String.IsNullOrEmpty(sortOrder) ? "imp_desc" : "";
            ViewBag.sortDue = String.IsNullOrEmpty(sortOrder) ? "duedate_desc" : "";
            ViewBag.sortComplete = String.IsNullOrEmpty(sortOrder) ? "complete_desc" : "";
            ViewBag.sortMyday = String.IsNullOrEmpty(sortOrder) ? "myday_desc" : "";
            ViewBag.sortAlpha = String.IsNullOrEmpty(sortOrder) ? "alpha_desc" : "";
            ViewBag.sortCreate = String.IsNullOrEmpty(sortOrder) ? "create_desc" : "";

            TaskPageModel model = new TaskPageModel();

            #region Page Tasks
            List<TaskModel> list = new List<TaskModel>();
            var gettasklists = _task.GetAll().Where(x => x.DueDate > DateTime.Today && x.ParentId == null && x.Status != (int)CustomToDoUtility.Statuses.close && x.IsDeleted == false).ToList();
            foreach(var gettask in gettasklists)
            {
                TaskModel task = new TaskModel();
                task.Id = gettask.Id;
                task.Title = gettask.Title;
                task.Priority = gettask.Priority;
                task.Status = gettask.Status;
                task.AssignedTo = gettask.AssignedTo;
                task.CreatedOn = gettask.CreatedOn;
                task.Creator = gettask.Creator;
                task.LastModifiedOn = gettask.LastModifiedOn;
                task.UpdatedBy = gettask.UpdatedBy;
                task.DueDate = gettask.DueDate;
                task.Comments = gettask.Comments;
                task.ParentId = gettask.ParentId ?? 0;

                var subtasklist = _task.GetAll().Where(x => x.DueDate > DateTime.Today && x.IsDeleted == false && x.ParentId == gettask.Id).ToList();
                List<TaskModel> sublist = new List<TaskModel>();
                foreach(var getsubtask in subtasklist)
                {
                    TaskModel subtask = new TaskModel();
                    subtask.Id = getsubtask.Id;
                    subtask.Title = getsubtask.Title;
                    subtask.Priority = getsubtask.Priority;
                    subtask.Status = getsubtask.Status;
                    subtask.AssignedTo = getsubtask.AssignedTo;
                    subtask.CreatedOn = getsubtask.CreatedOn;
                    subtask.Creator = getsubtask.Creator;
                    subtask.LastModifiedOn = getsubtask.LastModifiedOn;
                    subtask.UpdatedBy = getsubtask.UpdatedBy;
                    subtask.DueDate = getsubtask.DueDate;
                    subtask.Comments = getsubtask.Comments;
                    sublist.Add(subtask);
                }
                task.SubTaskList = sublist;

                list.Add(task);
            }

            foreach(var listitem in list)
            {
                listitem.SubTaskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id).Count();
                listitem.taskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id && l.Status == (int)CustomToDoUtility.Statuses.close).Count();
            }

            switch(sortOrder)
            {
                case "imp_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Priority).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "duedate_desc":
                    try
                    {
                        list = list.OrderBy(x => x.DueDate).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "complete_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Status).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "myday_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.DueDate).ThenBy(x => x.DueDate == DateTime.Today).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "alpha_desc":
                    try
                    {
                        list = list.OrderBy(x => x.Title).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "create_desc":
                    try
                    {
                        list = list.OrderBy(x => x.CreatedOn).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                default:
                    break;
            }
            model.PageTasks = list;
            #endregion

            model.TotalTasks = _task.GetAll().Where(x => x.IsDeleted == false && x.ParentId == null).Select(x => new TaskModel()
            {
                Id = x.Id,
                Title = x.Title,
                Priority = x.Priority,
                Status = x.Status,
                AssignedTo = x.AssignedTo,
                CreatedOn = x.CreatedOn,
                Creator = x.Creator,
                LastModifiedOn = x.LastModifiedOn,
                UpdatedBy = x.UpdatedBy,
                DueDate = x.DueDate,
                Comments = x.Comments

            }).OrderByDescending(x => x.CreatedOn).ToList();

            return View(model);
        }

        public ActionResult Important(string sortOrder)
        {
            ViewBag.sortImportant = String.IsNullOrEmpty(sortOrder) ? "imp_desc" : "";
            ViewBag.sortDue = String.IsNullOrEmpty(sortOrder) ? "duedate_desc" : "";
            ViewBag.sortComplete = String.IsNullOrEmpty(sortOrder) ? "complete_desc" : "";
            ViewBag.sortMyday = String.IsNullOrEmpty(sortOrder) ? "myday_desc" : "";
            ViewBag.sortAlpha = String.IsNullOrEmpty(sortOrder) ? "alpha_desc" : "";
            ViewBag.sortCreate = String.IsNullOrEmpty(sortOrder) ? "create_desc" : "";

            TaskPageModel model = new TaskPageModel();

            #region Page Tasks
            List<TaskModel> list = new List<TaskModel>();
            var gettasklists = _task.GetAll().Where(x => x.Priority == (int)CustomToDoUtility.Priorities.Important && x.ParentId == null && x.IsDeleted == false).ToList();
            foreach(var gettask in gettasklists)
            {
                TaskModel task = new TaskModel();
                task.Id = gettask.Id;
                task.Title = gettask.Title;
                task.Priority = gettask.Priority;
                task.Status = gettask.Status;
                task.AssignedTo = gettask.AssignedTo;
                task.CreatedOn = gettask.CreatedOn;
                task.Creator = gettask.Creator;
                task.LastModifiedOn = gettask.LastModifiedOn;
                task.UpdatedBy = gettask.UpdatedBy;
                task.DueDate = gettask.DueDate;
                task.Comments = gettask.Comments;
                task.ParentId = gettask.ParentId ?? 0;

                var subtasklist = _task.GetAll().Where(x => x.Priority == (int)CustomToDoUtility.Priorities.Important && x.ParentId == null && x.IsDeleted == false && x.ParentId == gettask.Id).ToList();
                List<TaskModel> sublist = new List<TaskModel>();
                foreach(var getsubtask in subtasklist)
                {
                    TaskModel subtask = new TaskModel();
                    subtask.Id = getsubtask.Id;
                    subtask.Title = getsubtask.Title;
                    subtask.Priority = getsubtask.Priority;
                    subtask.Status = getsubtask.Status;
                    subtask.AssignedTo = getsubtask.AssignedTo;
                    subtask.CreatedOn = getsubtask.CreatedOn;
                    subtask.Creator = getsubtask.Creator;
                    subtask.LastModifiedOn = getsubtask.LastModifiedOn;
                    subtask.UpdatedBy = getsubtask.UpdatedBy;
                    subtask.DueDate = getsubtask.DueDate;
                    subtask.Comments = getsubtask.Comments;
                    sublist.Add(subtask);
                }
                task.SubTaskList = sublist;

                list.Add(task);
            }

            foreach(var listitem in list)
            {
                listitem.SubTaskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id).Count();
                listitem.taskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id && l.Status == (int)CustomToDoUtility.Statuses.close).Count();
            }

            switch(sortOrder)
            {
                case "imp_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Priority).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "duedate_desc":
                    try
                    {
                        list = list.OrderBy(x => x.DueDate).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "complete_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Status).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "myday_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.DueDate).ThenBy(x => x.DueDate == DateTime.Today).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "alpha_desc":
                    try
                    {
                        list = list.OrderBy(x => x.Title).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "create_desc":
                    try
                    {
                        list = list.OrderBy(x => x.CreatedOn).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                default:
                    break;
            }
            model.PageTasks = list;
            #endregion           

            model.TotalTasks = _task.GetAll().Where(x => x.IsDeleted == false && x.ParentId == null).Select(x => new TaskModel()
            {
                Id = x.Id,
                Title = x.Title,
                Priority = x.Priority,
                Status = x.Status,
                AssignedTo = x.AssignedTo,
                CreatedOn = x.CreatedOn,
                Creator = x.Creator,
                LastModifiedOn = x.LastModifiedOn,
                UpdatedBy = x.UpdatedBy,
                DueDate = x.DueDate,
                Comments = x.Comments

            }).OrderByDescending(x => x.CreatedOn).ToList();

            Session["TaskList"] = model;
            return View(model);
        }

        public ActionResult Task(string sortOrder)
        {
            Session["TaskList"] = null;
            Session["attachmentList"] = null;
            ViewBag.sortImportant = String.IsNullOrEmpty(sortOrder) ? "imp_desc" : "";
            ViewBag.sortDue = String.IsNullOrEmpty(sortOrder) ? "duedate_desc" : "";
            ViewBag.sortComplete = String.IsNullOrEmpty(sortOrder) ? "complete_desc" : "";
            ViewBag.sortMyday = String.IsNullOrEmpty(sortOrder) ? "myday_desc" : "";
            ViewBag.sortAlpha = String.IsNullOrEmpty(sortOrder) ? "alpha_desc" : "";
            ViewBag.sortCreate = String.IsNullOrEmpty(sortOrder) ? "create_desc" : "";
            TaskPageModel model = new TaskPageModel();

            #region Page Tasks
            List<TaskModel> list = new List<TaskModel>();
            var gettasklists = _task.GetAll().Where(x => x.IsDeleted == false && x.ParentId == null).ToList();
            foreach(var gettask in gettasklists)
            {
                TaskModel task = new TaskModel();
                task.Id = gettask.Id;
                task.Title = gettask.Title;
                task.Priority = gettask.Priority;
                task.Status = gettask.Status;
                task.AssignedTo = gettask.AssignedTo;
                task.CreatedOn = gettask.CreatedOn;
                task.Creator = gettask.Creator;
                task.LastModifiedOn = gettask.LastModifiedOn;
                task.UpdatedBy = gettask.UpdatedBy;
                task.DueDate = gettask.DueDate;
                task.Comments = gettask.Comments;
                task.ParentId = gettask.ParentId ?? 0;

                var subtasklist = _task.GetAll().Where(x => x.IsDeleted == false && x.ParentId == gettask.Id).ToList();
                List<TaskModel> sublist = new List<TaskModel>();
                foreach(var getsubtask in subtasklist)
                {
                    TaskModel subtask = new TaskModel();
                    subtask.Id = getsubtask.Id;
                    subtask.Title = getsubtask.Title;
                    subtask.Priority = getsubtask.Priority;
                    subtask.Status = getsubtask.Status;
                    subtask.AssignedTo = getsubtask.AssignedTo;
                    subtask.CreatedOn = getsubtask.CreatedOn;
                    subtask.Creator = getsubtask.Creator;
                    subtask.LastModifiedOn = getsubtask.LastModifiedOn;
                    subtask.UpdatedBy = getsubtask.UpdatedBy;
                    subtask.DueDate = getsubtask.DueDate;
                    subtask.Comments = getsubtask.Comments;
                    subtask.ParentId = getsubtask.ParentId.Value;
                    sublist.Add(subtask);
                }
                task.SubTaskList = sublist;

                list.Add(task);
            }

            foreach(var listitem in list)
            {
                listitem.SubTaskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id).Count();
                listitem.taskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id && l.Status == (int)CustomToDoUtility.Statuses.close).Count();
            }

            switch(sortOrder)
            {
                case "imp_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Priority).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "duedate_desc":
                    try
                    {
                        list = list.OrderBy(x => x.DueDate).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "complete_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Status).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "myday_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.DueDate).ThenBy(x => x.DueDate == DateTime.Today).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "alpha_desc":
                    try
                    {
                        list = list.OrderBy(x => x.Title).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "create_desc":
                    try
                    {
                        list = list.OrderBy(x => x.CreatedOn).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                default:
                    break;
            }
            model.PageTasks = list;
            #endregion

            model.TotalTasks = _task.GetAll().Where(x => x.IsDeleted == false && x.ParentId == null).Select(x => new TaskModel()
            {
                Id = x.Id,
                Title = x.Title,
                Priority = x.Priority,
                Status = x.Status,
                AssignedTo = x.AssignedTo,
                CreatedOn = x.CreatedOn,
                Creator = x.Creator,
                LastModifiedOn = x.LastModifiedOn,
                UpdatedBy = x.UpdatedBy,
                DueDate = x.DueDate,
                Comments = x.Comments
            }).OrderByDescending(x => x.CreatedOn).ToList();

            Session["TaskList"] = model;
            return View(model);
        }

        public ActionResult Assigned(string sortOrder)
        {
            ViewBag.sortImportant = String.IsNullOrEmpty(sortOrder) ? "imp_desc" : "";
            ViewBag.sortDue = String.IsNullOrEmpty(sortOrder) ? "duedate_desc" : "";
            ViewBag.sortComplete = String.IsNullOrEmpty(sortOrder) ? "complete_desc" : "";
            ViewBag.sortMyday = String.IsNullOrEmpty(sortOrder) ? "myday_desc" : "";
            ViewBag.sortAlpha = String.IsNullOrEmpty(sortOrder) ? "alpha_desc" : "";
            ViewBag.sortCreate = String.IsNullOrEmpty(sortOrder) ? "create_desc" : "";
            TaskPageModel model = new TaskPageModel();

            #region Page Tasks
            List<TaskModel> list = new List<TaskModel>();
            var gettasklists = _task.GetAll().Where(x => x.AssignedTo.HasValue && x.AssignedTo.Value == CustomToDoUtility.CurrentLoginUser.Id && x.IsDeleted == false && x.ParentId == null).ToList();
            foreach(var gettask in gettasklists)
            {
                TaskModel task = new TaskModel();
                task.Id = gettask.Id;
                task.Title = gettask.Title;
                task.Priority = gettask.Priority;
                task.Status = gettask.Status;
                task.AssignedTo = gettask.AssignedTo;
                task.CreatedOn = gettask.CreatedOn;
                task.Creator = gettask.Creator;
                task.LastModifiedOn = gettask.LastModifiedOn;
                task.UpdatedBy = gettask.UpdatedBy;
                task.DueDate = gettask.DueDate;
                task.Comments = gettask.Comments;
                task.ParentId = gettask.ParentId ?? 0;

                var subtasklist = _task.GetAll().Where(x => x.AssignedTo.HasValue && x.AssignedTo.Value == CustomToDoUtility.CurrentLoginUser.Id && x.IsDeleted == false && x.ParentId == gettask.Id).ToList();
                List<TaskModel> sublist = new List<TaskModel>();
                foreach(var getsubtask in subtasklist)
                {
                    TaskModel subtask = new TaskModel();
                    subtask.Id = getsubtask.Id;
                    subtask.Title = getsubtask.Title;
                    subtask.Priority = getsubtask.Priority;
                    subtask.Status = getsubtask.Status;
                    subtask.AssignedTo = getsubtask.AssignedTo;
                    subtask.CreatedOn = getsubtask.CreatedOn;
                    subtask.Creator = getsubtask.Creator;
                    subtask.LastModifiedOn = getsubtask.LastModifiedOn;
                    subtask.UpdatedBy = getsubtask.UpdatedBy;
                    subtask.DueDate = getsubtask.DueDate;
                    subtask.Comments = getsubtask.Comments;
                    sublist.Add(subtask);
                }
                task.SubTaskList = sublist;

                list.Add(task);
            }

            foreach(var listitem in list)
            {
                listitem.SubTaskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id).Count();
                listitem.taskCount = _task.GetAll().Where(l => l.IsDeleted == false && l.ParentId == listitem.Id && l.Status == (int)CustomToDoUtility.Statuses.close).Count();

            }

            switch(sortOrder)
            {
                case "imp_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Priority).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "duedate_desc":
                    try
                    {
                        list = list.OrderBy(x => x.DueDate).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "complete_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.Status).Where(x => x.AssignedTo.HasValue && x.AssignedTo.Value == CustomToDoUtility.CurrentLoginUser.Id).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "myday_desc":
                    try
                    {
                        list = list.OrderByDescending(x => x.DueDate).ThenBy(x => x.DueDate == DateTime.Today).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "alpha_desc":
                    try
                    {
                        list = list.OrderBy(x => x.Title).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                case "create_desc":
                    try
                    {
                        list = list.OrderBy(x => x.CreatedOn).ToList();
                    }
                    catch(Exception ex)
                    {
                        Response.Write(ex);
                    }
                    break;
                default:
                    break;
            }
            model.PageTasks = list;
            #endregion

            model.TotalTasks = _task.GetAll().Where(x => x.IsDeleted == false && x.ParentId == null).Select(x => new TaskModel()
            {
                Id = x.Id,
                Title = x.Title,
                Priority = x.Priority,
                Status = x.Status,
                AssignedTo = x.AssignedTo,
                CreatedOn = x.CreatedOn,
                Creator = x.Creator,
                LastModifiedOn = x.LastModifiedOn,
                UpdatedBy = x.UpdatedBy,
                DueDate = x.DueDate,
                Comments = x.Comments

            }).OrderByDescending(x => x.CreatedOn).ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var task = _task.GetAll().Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
            if(task != null)
            {
                task.IsDeleted = true;
                _task.Edit(task);
                _task.Save();
            }
            return RedirectToAction("Task");
        }

        [HttpPost]
        public ActionResult DeleteSubTask(int id)
        {
            TaskPageModel model = null;
            var task = _task.GetAll().Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
            if(task != null)
            {
                task.IsDeleted = true;
                _task.Edit(task);
                _task.Save();
                model = (TaskPageModel)Session["TaskList"];
                //var item = model.PageTasks.SingleOrDefault(x => x.Id == id);
                //if (item!=null)
                //{
                //    model.PageTasks.Remove(item);
                //}
                foreach (var item in model.PageTasks)
                {
                    if (item.Id == task.ParentId)
                    {
                        foreach (var subtaskitem in item.SubTaskList)
                        {
                            item.SubTaskList.Remove(subtaskitem);
                            int count = subtaskitem.SubTaskList == null ? 0 : subtaskitem.SubTaskList.Count();
                            item.SubTaskCount = count;
                            break;
                        }
                        break;
                    }
                }
                Session["TaskList"] = model;
                return PartialView("_CenterPageContent", model);
            }
            //return RedirectToAction("Task");
            return PartialView("_CenterPageContent", model);
        }

        [HttpPost]
        public JsonResult AddComment(int id, string comment)
        {
            if(id > 0)
            {
                var task = _task.FindBy(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
                if(task != null)
                {
                    task.Comments = comment;
                    _task.Edit(task);
                    _task.Save();
                    return Json("success");
                }
            }
            return Json("Fail");
        }

        [HttpPost]
        public JsonResult EditTitle(int id, string title)
        {
            if(id > 0)
            {
                var task = _task.FindBy(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
                if(task != null)
                {
                    task.Title = title;
                    _task.Edit(task);
                    _task.Save();
                    return Json("success");
                }
            }
            return Json("Fail");
        }

        [HttpPost]
        public JsonResult Addtomyday(int id)
        {
            if(id > 0)
            {
                var task = _task.FindBy(x => x.Id == id).FirstOrDefault();
                if(task != null)
                {
                    task.DueDate = DateTime.Today;
                    _task.Edit(task);
                    _task.Save();
                    return Json("success");
                }
            }
            return Json("Fail");
        }

        [HttpPost]
        public ActionResult Ischeck(int id, Boolean ischecked)
        {
            TaskPageModel model = null;
            if (id > 0)
            {
                var task = _task.FindBy(x => x.Id == id).FirstOrDefault();
                if(task != null)
                {
                    if(ischecked.Equals(true))
                    {
                        task.Status = (int)CustomToDoUtility.Statuses.close;
                        _task.Edit(task);
                    }
                    else
                    {
                        task.Status = (int)CustomToDoUtility.Statuses.open;
                        _task.Edit(task);
                    }
                    _task.Save();
                    //return Json("success");
                    model = (TaskPageModel)Session["TaskList"];
                    if (task.ParentId==0 || task.ParentId==null)
                    {
                        foreach (var item in model.PageTasks)
                        {
                            if (item.Id == task.Id)
                            {
                                item.Status = task.Status;
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in model.PageTasks)
                        {
                            if (item.Id == task.ParentId)
                            {
                                foreach (var subtaskitem in item.SubTaskList)
                                {
                                    if (subtaskitem.Id == id)
                                    {
                                        subtaskitem.Status = task.Status;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    
                    Session["TaskList"] = model;
                    return PartialView("_CenterPageContent", model);
                }
            }
            //return Json("Fail");
            return PartialView("_CenterPageContent", model);
        }

        [HttpPost]
        public ActionResult IsImportant(int id)
        {
            TaskPageModel model = null;
            if (id > 0)
            {
                var task = _task.FindBy(x => x.Id == id).FirstOrDefault();
                if(task != null)
                {
                    if(task.Priority == (int)CustomToDoUtility.Priorities.Important)
                    {
                        task.Priority = (int)CustomToDoUtility.Priorities.Normal;
                        _task.Edit(task);
                    }
                    else
                    {
                        task.Priority = (int)CustomToDoUtility.Priorities.Important;
                        _task.Edit(task);
                    }
                    _task.Save();
                    model = (TaskPageModel)Session["TaskList"];
                    foreach (var item in model.PageTasks)
                    {
                        if(item.Id==task.Id)
                        {
                            item.Priority = task.Priority;
                            foreach (var totalitem in model.TotalTasks)
                            {
                                if (totalitem.Id ==task.Id)
                                {
                                    totalitem.Priority = task.Priority;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    Session["TaskList"] = model;
                    return PartialView("_CenterPageContent", model);
                }
            }
            return PartialView("_CenterPageContent", model);
        }

        [HttpPost]
        public JsonResult AddSteps(int id,string step)
        {
            if(id > 0)
            {
                var task = _task.FindBy(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
                if(task != null)
                {
                    task.Title = step;
                    task.Priority = (int)CustomToDoUtility.Priorities.Normal;
                    task.Status = (int)CustomToDoUtility.Statuses.open;
                    task.ParentId = id;
                    task.IsDeleted = false;
                    task.Creator = CustomToDoUtility.CurrentLoginUser.Id;
                    task.CreatedOn = DateTime.Now;
                    _task.Add(task);
                    _task.Save();
                    return Json("success");
                }
            }
            return Json("Fail");
        }
        
        [HttpPost]
        public ActionResult GetSidePanelData(int id)
        {
            TaskPageModel model = null;
            Session["attachmentList"] = null;
            if (id > 0)
            {
                model = (TaskPageModel)Session["TaskList"];
                foreach (var item in model.PageTasks)
                {
                    if (item.Id == id)
                    {
                        //List<TaskAttachment> attachmentdata = _taskAttachment.GetTaskAttachmentsById(id);
                        //if (attachmentdata.Count() > 0)
                        //{
                        //    Session["attachmentList"] = attachmentdata;
                        //}
                        return PartialView("SidePanel", item);
                    }
                }

            }
            return PartialView("SidePanel", new TaskModel());
        }

        [HttpPost]
        public JsonResult Addtask(string title, string type)
        {
            if (type == "Task")
            {
                if (ModelState.IsValid)
                {
                    DAL.Task task = new DAL.Task();
                    task.Title = title;
                    task.Priority = (int)CustomToDoUtility.Priorities.Normal;
                    task.Status = (int)CustomToDoUtility.Statuses.open;
                    task.Creator = CustomToDoUtility.CurrentLoginUser.Id;
                    task.CreatedOn = DateTime.Now;
                    task.IsDeleted = false;
                    task.Comments = "";
                    _task.Add(task);
                    _task.Save();
                }
                else
                {
                    return Json("fail");
                }
            }
            if (type == "Planned")
            {
                if (ModelState.IsValid)
                {
                    DAL.Task task = new DAL.Task();
                    task.Title = title;
                    task.Priority = (int)CustomToDoUtility.Priorities.Normal;
                    task.Status = (int)CustomToDoUtility.Statuses.open;
                    task.Creator = CustomToDoUtility.CurrentLoginUser.Id;
                    task.CreatedOn = DateTime.Now;
                    task.DueDate = DateTime.Now.AddDays(1);
                    task.IsDeleted = false;
                    task.Comments = "";
                    _task.Add(task);
                    _task.Save();
                }
                else
                {
                    return Json("fail");
                }
            }
            if (type == "MyDay")
            {
                if (ModelState.IsValid)
                {
                    DAL.Task task = new DAL.Task();
                    task.Title = title;
                    task.Priority = (int)CustomToDoUtility.Priorities.Normal;
                    task.Status = (int)CustomToDoUtility.Statuses.open;
                    task.Creator = CustomToDoUtility.CurrentLoginUser.Id;
                    task.CreatedOn = DateTime.Now;
                    task.DueDate = DateTime.Today;
                    task.IsDeleted = false;
                    task.Comments = "";
                    _task.Add(task);
                    _task.Save();
                }
                else
                {
                    return Json("fail");
                }
            }
            if (type == "Important")
            {
                if (ModelState.IsValid)
                {
                    DAL.Task task = new DAL.Task();
                    task.Title = title;
                    task.Priority = (int)CustomToDoUtility.Priorities.Important;
                    task.Status = (int)CustomToDoUtility.Statuses.open;
                    task.Creator = CustomToDoUtility.CurrentLoginUser.Id;
                    task.CreatedOn = DateTime.Now;
                    task.IsDeleted = false;
                    task.Comments = "";
                    _task.Add(task);
                    _task.Save();
                }
                else
                {
                    return Json("fail");
                }
            }
            return Json("success");
        }
        
        public ActionResult LoadMenuBar()
        {
            TaskPageModel model = null;
            model = (TaskPageModel)Session["TaskList"];
            return PartialView("_MenuBar", model);
        }

        [HttpPost]
        public ActionResult UploadAttachment(int taskid)
        {
            string physicalPath = HttpContext.Server.MapPath(@"~\Uploads\");

            UploadedFile file = null;

            try
            {
                foreach (string item in Request.Files)
                {
                    var fileContent = Request.Files[item];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        fileContent.SaveAs(physicalPath + System.IO.Path.GetFileName(fileContent.FileName));

                        file = new UploadedFile();
                        file.FileName = fileContent.FileName;
                        file.FilePath = physicalPath + fileContent.FileName;
                        file.FileSize = fileContent.ContentLength;
                        file.FileType = fileContent.ContentType;

                        FileInfo fi = new FileInfo(physicalPath + System.IO.Path.GetFileName(fileContent.FileName));

                        file.FileExtension = fi.Extension.Replace(".", "");

                        TaskAttachment attach = new TaskAttachment();
                        attach.FileExtension = file.FileExtension;
                        attach.FileName = file.FileName;
                        attach.FilePath = file.FilePath;
                        attach.FileSize = file.FileSize;
                        attach.TaskId = taskid;
                        _taskAttachment.Add(attach);
                        _taskAttachment.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }

            return Json(file, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult RemoveAttachment(int aID, int tID)
        {
            var attachment = _taskAttachment.GetAll().Where(x => x.Id == aID && x.TaskId == tID).FirstOrDefault();
            if (attachment != null)
            {
                _taskAttachment.Delete(attachment);
                _taskAttachment.Save();
            }

            List<TaskAttachment> attachmentdata = _taskAttachment.GetTaskAttachmentsById(tID);
            if (attachmentdata.Count() > 0)
            {
                Session["attachmentList"] = attachmentdata;
            }

            return PartialView("_AttachmentData", attachmentdata);
        }
    }
}


