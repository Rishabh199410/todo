﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomToDo.Web.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Priority { get; set; }
        public int Status { get; set; }
        public Nullable<int> AssignedTo { get; set; }
        public int Creator { get; set; }
        public string Comments { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<System.DateTime> LastModifiedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public int ParentId { get; set; }
        public int SubTaskCount { get; set; }
        public List<TaskModel> SubTaskList { get; set; }
        public int taskCount { get; set; }


    }

    public class TaskPageModel
    {
        public List<TaskModel> PageTasks { get; set; }
        public List<TaskModel> TotalTasks { get; set; }
    }
}