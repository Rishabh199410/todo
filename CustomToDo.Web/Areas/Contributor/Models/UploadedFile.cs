﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomToDo.Web.Areas.Contributor.Models
{
    public class UploadedFile
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public int FileSize { get; set; }

        public string FileExtension { get; set; }
    }
}