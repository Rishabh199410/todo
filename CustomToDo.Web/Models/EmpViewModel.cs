﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomToDo.Web.Models
{
    public class EmpViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}