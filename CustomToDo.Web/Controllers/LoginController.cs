﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomToDo.BAL.Repository;
using CustomToDo.Web.HelperClass;
using CustomToDo.Web.Models;

namespace CustomToDo.Web.Controllers
{
    public class LoginController : Controller
    {
        public IEmp _empServices;
        public IAppUser _appUser;


        public LoginController(IEmp empServices,IAppUser user)
        {
            _empServices = empServices;
            _appUser = user;
        }

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public JsonResult LoginCheck(string username, string password)
        {
            DAL.AppUser appuser;

            appuser = _appUser.FindBy(x => x.Username == username && x.Password == password).FirstOrDefault();
            if (appuser != null)
            {
                if (appuser.RoleId == (int)CustomToDoUtility.Role.Admin)
                {
                    Session["CurrentUser"] = appuser;
                    return Json("Admin");
                }
                else if (appuser.RoleId == (int)CustomToDoUtility.Role.Manager)
                {
                    Session["CurrentUser"] = appuser;
                    return Json("Manager");
                }
                else if (appuser.RoleId == (int)CustomToDoUtility.Role.Contributor)
                {
                    Session["CurrentUser"] = appuser;
                    return Json("Contributor");
                }
                else
                {
                    return Json("fail");
                }
            }
            else
            {
                return Json("fail");
            }
            }

        }
    }
