﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomToDo.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult MyDay()
        {
            return View();
        }
        public ActionResult Important()
        {
            return View();
        }
        public ActionResult Planned()
        {
            return View();
        }
        public ActionResult Assigned()
        {
            return View();
        }
        public ActionResult Task()
        {
            return View();
        }

    }
}